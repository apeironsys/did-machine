DID Machine
=========

The DID Machine is an application for procuring direct inward dial phone numbers (DIDs), building FreeSWITCH servers and centrally managing the functional configuration for the phone numbers routed to any of the servers in the FreeSWITCH fabric.


Installation
--------------

Before installing the application you will need to load git submodules:

	$ git submodule update --init --recursive

Alternatively you can clone the repository with `--recursive` option:

	$ git clone --recursive

#### With Vagrant

	$ vagrant up

#### With Ansible

_Only tested with Debian 8 Jessie host._

Use `webapp_playbook.yml` playbook. Nginx and uWSGI Emperor services will be installed and configured on the remote host.
WSGI process log file will be located in `/var/log/uwsgi/did_machine.log`.

#### Manually

	$ pip install -r requirements.txt  # install Python requirements
	$ python manage.py migrate  # create sqlite database
	$ python manage.py runserver  # run the built-in (single-threaded) Django webserver


Obtaining trunk group and API credentials
--------------

1. Order an inbound or bidirectional SIP trunk at [https://apeiron.io/order](https://apeiron.io/order)
    * Make sure to configure the trunk with public IPs of servers that will be hosting FreeSwitch
2. Generate an API key in [Developer Portal](https://dashboard.apeiron.io/dashboard/developers/)


Configuration
--------------

Application configuration is located in [config.yml](config.yml). You will need to edit this file before starting the application.


Provisioning FreeSwitch servers
--------------

DID Machine utilizes Ansible to install and configure FreeSwitch on a remote server.

Requirements:

* Only Debian 8 Jessie is fully supported at this time. Ubuntu 14.04 _should_ work but it hasn't been fully tested.
* Servers must to be configured in your `~/.ssh/config` file for password-less authentication.

Deploy new servers using the following command:

	$ python manage.py provision_freeswitch --help



License
-------

MIT


Author
------------------

apeiron.io <[info@apeiron.io](mailto:info@apeiron.io)> [apeiron.io](https://apeiron.io/)
