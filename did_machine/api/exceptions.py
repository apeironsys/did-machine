class SearchNumbersException(Exception):
    pass


class ListReservedNumbersException(Exception):
    pass


class ReserveNumberException(Exception):
    pass


class UnreserveNumberException(Exception):
    pass


class OrderNumbersException(Exception):
    pass


class ListNumbersException(Exception):
    pass
