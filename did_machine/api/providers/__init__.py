from django.conf import settings


class Provider(object):
    def __init__(self, config):
        self.config = config

    def search_numbers(self, count=5):
        raise NotImplemented()

    def list_reserved_numbers(self):
        raise NotImplemented()

    def reserve_number(self, number):
        raise NotImplemented()

    def unreserve_number(self, number):
        raise NotImplemented()

    def order_numbers(self, numbers):
        raise NotImplemented()

    def list_numbers(self):
        raise NotImplemented()


def get_provider_driver():
    """
    Returns provider driver class instance
    :return: Driver class instance
    """
    provider_config = settings.EXT_CONFIG.get('provider')

    module_name, class_name = provider_config['driver'].split(':')
    mod = __import__(module_name, fromlist=[class_name])

    return getattr(mod, class_name)(provider_config)


PROVIDER_DRIVER = get_provider_driver()
