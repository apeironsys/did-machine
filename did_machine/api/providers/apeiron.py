import random
import string
from logging import getLogger

import requests
from django.core.cache import cache

from did_machine.api.exceptions import SearchNumbersException, ListReservedNumbersException, ReserveNumberException, \
    UnreserveNumberException, OrderNumbersException, ListNumbersException
from did_machine.api.providers import Provider

log = getLogger(__name__)


class Apeiron(Provider):
    def __init__(self, config):
        super(Apeiron, self).__init__(config)

        self.api_url = self.config.get('api_url', 'https://api.apeiron.io/v2')
        log.debug('API URL: %s', self.api_url)

        self.auth = (self.config['api_user'], self.config['api_key'])
        log.debug('API auth: %s', self.auth)

        self.reservation_id = self.config.get('reservation_id',
                                              'DIDMACHINE%s' % ''.join(
                                                  random.SystemRandom().choice(string.ascii_uppercase + string.digits)
                                                  for _ in range(8)))
        log.debug('Reservation ID: %s', self.reservation_id)

    def search_numbers(self, count=5):
        npa_list = self.config['npa_list']
        random.shuffle(npa_list)
        npa = npa_list[0]

        log.info('Querying apeiron API for new numbers in %s NPA', npa)

        r = requests.get(self.api_url + '/numbers/search/npa/' + str(npa) + '?count=' + str(count),
                         auth=self.auth)

        if r.status_code != requests.codes.ok:
            log.error('apeiron API responded with status code %s', r.status_code)
            log.debug('apeiron API error: %s', r.content)
            raise SearchNumbersException('Could not find new numbers')

        return r.json().get('numbers', [])

    def list_reserved_numbers(self):
        log.debug('Trying to retrieve reserved numbers from cache')
        numbers = cache.get('reserved_numbers')

        if numbers is None:
            log.info('Retrieving reserved numbers from apeiron API')
            r = requests.get(self.api_url + '/numbers/reservations/' + str(self.reservation_id),
                             auth=self.auth)

            if r.status_code == 404:
                # reservation not found
                log.warning('Reservation not found')
                return []

            if r.status_code != requests.codes.ok:
                log.error('apeiron API responded with status code %s', r.status_code)
                log.debug('apeiron API error: %s', r.content)
                raise ListReservedNumbersException('Could not list reserved numbers')

            numbers = r.json()
            cache.set('reserved_numbers', numbers, 60 * 5)

        return numbers

    def reserve_number(self, number):
        r = requests.get(self.api_url + '/numbers/reservations/' + str(self.reservation_id) + '/' + str(number),
                         auth=self.auth)

        if r.status_code != requests.codes.ok:
            log.error('apeiron API responded with status code %s', r.status_code)
            log.debug('apeiron API error: %s', r.content)
            raise ReserveNumberException('Could not reserve number')

        cache.delete('reserved_numbers')

        return r.json()

    def unreserve_number(self, number):
        r = requests.delete(
            self.api_url + '/numbers/reservations/' + str(self.reservation_id) + '/' + str(number),
            auth=self.auth)

        if r.status_code != requests.codes.ok:
            log.error('apeiron API responded with status code %s', r.status_code)
            log.debug('apeiron API error: %s', r.content)
            raise UnreserveNumberException('Could not unreserve number')

        cache.delete('reserved_numbers')

        return r.json()

    def order_numbers(self, numbers):
        trunk_group = self.config['trunk_group']
        order_data = [dict(number=n) for n in numbers]

        r = requests.post(
            self.api_url + '/voice/trunk_groups/' + str(trunk_group) + '/add_numbers',
            json=order_data,
            auth=self.auth)

        if r.status_code != requests.codes.ok:
            log.error('apeiron API responded with status code %s', r.status_code)
            log.debug('apeiron API error: %s', r.content)
            raise OrderNumbersException('Could not order numbers')

        cache.delete('reserved_numbers')

        return r.json().get('service_order_id')

    def list_numbers(self):
        trunk_group = self.config['trunk_group']
        r = requests.get(self.api_url + '/voice/trunk_groups/' + str(trunk_group),
                         auth=self.auth)

        if r.status_code != requests.codes.ok:
            log.error('apeiron API responded with status code %s', r.status_code)
            log.debug('apeiron API error: %s', r.content)
            raise ListNumbersException('Could not list numbers')

        return set([n['number'] for n in r.json().get('numbers')])
