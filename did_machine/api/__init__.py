from logging import getLogger

from did_machine.api.providers import PROVIDER_DRIVER
from did_machine.models import Number

log = getLogger(__name__)


def search_numbers(count=5):
    return PROVIDER_DRIVER.search_numbers(count=count)


def list_reserved_numbers():
    return PROVIDER_DRIVER.list_reserved_numbers()


def reserve_number(number):
    return PROVIDER_DRIVER.reserve_number(number=number)


def unreserve_number(number):
    return PROVIDER_DRIVER.unreserve_number(number=number)


def order_numbers(numbers):
    return PROVIDER_DRIVER.order_numbers(numbers=numbers)


def list_numbers():
    return PROVIDER_DRIVER.list_numbers()


def sync_numbers():
    numbers = list_numbers()
    log.debug('Got numbers: %s', ', '.join(numbers))

    existing_numbers = set(Number.objects.all().values_list('number', flat=True))
    new_numbers = numbers - existing_numbers

    log.info('New numbers to add: %s', ', '.join(new_numbers))

    for number in new_numbers:
        Number.objects.create(number=number,
                              order='sync',
                              voicemail=True)
