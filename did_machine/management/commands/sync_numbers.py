from django.core.management.base import BaseCommand

from did_machine.api import sync_numbers


class Command(BaseCommand):
    help = 'Sync numbers from apeiron.io'

    def handle(self, *args, **options):
        sync_numbers()
