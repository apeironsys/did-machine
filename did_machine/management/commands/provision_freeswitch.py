import os
import subprocess
from logging import getLogger
from tempfile import NamedTemporaryFile

from ansible.plugins.callback import json
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse
from prompter import yesno

from did_machine.models import Device

log = getLogger(__name__)


class Command(BaseCommand):
    help = 'Provisions FreeSwitch onto a remote host'

    def add_arguments(self, parser):
        parser.add_argument('hostname', help='FQDN or IP of the device as configured in your SSH config')
        parser.add_argument('label', nargs='?', help='Optional device label')
        parser.add_argument('-b', '--batch', action='store_true',
                            help='Suppress all interactive prompts.')
        parser.add_argument('-d', '--freeswitch-domain',
                            help='FreeSWITCH domain (defaults to hostname)')

    def handle(self, *args, **options):
        # check if device already exists
        if Device.objects.filter(hostname=options['hostname']).exists():
            log.warning('Device already exists in the database')

            provision = (options['batch'] or
                         yesno('Device already exists in the database. Would you like to provision again?'))
            if not provision:
                return

            device = Device.objects.get(hostname=options['hostname'])
        else:
            device = Device.objects.create(hostname=options['hostname'])
            log.debug('Created device record id=%s', device.pk)

        device.device_label = options['label'] or options['hostname']
        device.fs_domain = options['freeswitch_domain'] or options['hostname']
        device.save(update_fields=['device_label', 'fs_domain'])

        log.info('Getting ready to install FreeSwitch on %s', device)

        # create inventory file
        hostlist_file = NamedTemporaryFile(prefix='freeswitch_hostlist', delete=False)
        hostlist_file.file.write("[freeswitch]\n%s\n" % device.hostname)
        hostlist_file.file.close()
        log.debug('Ansible inventory written to %s', hostlist_file.name)

        # extra vars
        playbook_extra_vars = settings.EXT_CONFIG.get('freeswitch')['playbook']
        playbook_extra_vars.update(dict(
            mod_curl_dialplan=settings.SITE_URL.rstrip('/') + reverse('freeswitch_dialplan'),
            mod_curl_directory=settings.SITE_URL.rstrip('/') + reverse('freeswitch_directory'),
            device_fs_domain=device.fs_domain,
            ext_rtp_ip=device.fs_domain,
            ext_sip_ip=device.fs_domain
        ))

        # create extra vars json file
        extra_vars = NamedTemporaryFile(prefix='freeswitch_extra_vars', delete=False)
        extra_vars.file.write(json.dumps(playbook_extra_vars))
        extra_vars.file.close()
        log.debug('Extra vars written to %s', extra_vars.name)

        command = settings.EXT_CONFIG.get('ansible')['playbook_command'] % {
            'inventory': hostlist_file.name,
            'extra_vars': extra_vars.name,
            'playbook': settings.ANSIBLE_FREESWITCH_PLAYBOOK
        }

        log.info('Executing Ansible')
        log.debug('Executing command: %s', command)

        p = subprocess.Popen(command, shell=True)
        p.wait()

        log.debug('Ansible command finished')

        os.unlink(hostlist_file.name)
        log.debug('Deleted inventory file %s', hostlist_file.name)

        if p.returncode == 0:
            log.info('FreeSwitch installed')

            device.provisioned = True
            device.save(update_fields=['provisioned'])
            log.debug('Marked device as provisioned')
        else:
            raise CommandError('FreeSwitch installation failed!')
