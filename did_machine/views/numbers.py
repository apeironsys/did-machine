import django_tables2 as tables
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import TemplateView

from did_machine.models import Number


class NumberTable(tables.Table):
    toggle_fields = ['fax', 'voicemail', 'conference']

    id = tables.Column(attrs={'th': {'width': '5%'}})
    number = tables.Column(attrs={'th': {'width': '20%'}})
    order = tables.Column(verbose_name='Order Number')

    fax = tables.TemplateColumn(template_name='elements/numbers_table_toggle.html',
                                attrs={'th': {'width': '10%', 'class': 'text-center'},
                                       'td': {'class': 'text-center'}})
    voicemail = tables.TemplateColumn(template_name='elements/numbers_table_toggle.html',
                                      attrs={'th': {'width': '10%', 'class': 'text-center'},
                                             'td': {'class': 'text-center'}})
    conference = tables.TemplateColumn(template_name='elements/numbers_table_toggle.html',
                                       attrs={'th': {'width': '10%', 'class': 'text-center'},
                                              'td': {'class': 'text-center'}})

    buttons = tables.TemplateColumn(template_name='elements/numbers_buttons.html',
                                    orderable=False,
                                    verbose_name='',
                                    attrs={'th': {'width': '10%'},
                                           'td': {'class': 'text-right'}})

    class Meta:
        model = Number


class NumbersPageView(TemplateView):
    template_name = 'numbers.html'

    def get(self, *args, **kwargs):
        if 'delete' in self.request.GET:
            get_object_or_404(Number, number=self.request.GET.get('delete')).delete()

            return redirect('numbers')

        def _toggle_field(field):
            number = get_object_or_404(Number, number=self.request.GET.get('toggle_%s' % field))
            for _field in NumberTable.toggle_fields:
                if field == _field:
                    setattr(number, field, not getattr(number, field))
                else:
                    setattr(number, _field, False)

            number.save(update_fields=NumberTable.toggle_fields)

        for field in NumberTable.toggle_fields:
            if 'toggle_%s' % field in self.request.GET:
                _toggle_field(field)
                return redirect('numbers')

        return super(NumbersPageView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(NumbersPageView, self).get_context_data(**kwargs)

        numbers = Number.objects.all()
        numbers_table = NumberTable(numbers)

        tables.RequestConfig(self.request).configure(numbers_table)

        context.update(dict(
            numbers=numbers_table,
            current_page='numbers'
        ))

        return context
