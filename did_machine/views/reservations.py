import random

import django_tables2 as tables
from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic import TemplateView

from did_machine.api import search_numbers, reserve_number, unreserve_number, list_reserved_numbers, order_numbers, \
    sync_numbers
from did_machine.models import Number


class ReservedNumbersTable(tables.Table):
    number = tables.Column(orderable=False)
    reservation_date = tables.Column(orderable=False)
    expires = tables.Column(orderable=False, verbose_name='Reservation Expiration Date')

    buttons = tables.TemplateColumn(template_name='elements/reservations_buttons.html',
                                    orderable=False,
                                    verbose_name='',
                                    attrs={'th': {'width': '10%'},
                                           'td': {'class': 'text-right'}})


class NumberReservationsView(TemplateView):
    template_name = 'reservations.html'

    def get(self, *args, **kwargs):
        if 'reserve' in self.request.GET:
            # search and reserve new number
            try:
                search = search_numbers(count=3)
            except Exception as e:
                messages.error(self.request, 'Error searching numbers: %s' % e)

            if len(search) == 0:
                messages.error(self.request, 'Did not find any numbers')
            else:
                try:
                    random.shuffle(search)
                    reserve_number(search[0])
                except Exception as e:
                    messages.error(self.request, 'Error reserving number %s: %s' % (search[0], e))

            return redirect('reservations')

        elif 'unreserve' in self.request.GET:
            try:
                unreserve_number(self.request.GET.get('unreserve'))
            except Exception as e:
                messages.error(self.request, 'Error unreserving number: %s' % e)
            return redirect('reservations')

        elif 'order' in self.request.GET:
            # order reserved numbers
            numbers = [x.get('number') for x in list_reserved_numbers()]

            try:
                order = order_numbers(numbers)
            except Exception as e:
                messages.error(self.request, 'Error ordering numbers: %s' % e)

            for number in numbers:
                Number.objects.create(number=number, order=order, voicemail=True)

            return redirect('numbers')

        elif 'sync' in self.request.GET:
            try:
                sync_numbers()
            except Exception as e:
                messages.error(self.request, 'Error syncing numbers: %s' % e)

            return redirect('numbers')

        return super(NumberReservationsView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(NumberReservationsView, self).get_context_data(**kwargs)

        try:
            numbers = list_reserved_numbers()
        except Exception as e:
            messages.error(self.request, 'Error retrieving reserved numbers: %s' % e)
            numbers = []

        numbers_table = ReservedNumbersTable(numbers)

        context.update(dict(
            numbers=numbers_table,
            current_page='reservations'
        ))

        return context
