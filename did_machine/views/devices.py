import django_tables2 as tables
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import TemplateView

from did_machine.models import Device


class DeviceTable(tables.Table):
    id = tables.Column(attrs={'th': {'width': '5%'}})
    device_label = tables.Column(attrs={'th': {'width': '25%'}})
    fs_domain = tables.Column(verbose_name='FreeSWITCH Domain', attrs={'th': {'width': '25%'}})
    provisioned = tables.BooleanColumn(attrs={'th': {'width': '15%', 'class': 'text-center'},
                                              'td': {'class': 'text-center'}})

    buttons = tables.TemplateColumn(template_name='elements/devices_buttons.html',
                                    orderable=False,
                                    verbose_name='',
                                    attrs={'th': {'width': '10%'},
                                           'td': {'class': 'text-right'}})

    class Meta:
        model = Device


class DevicesPageView(TemplateView):
    template_name = 'devices.html'

    def get(self, *args, **kwargs):
        if 'delete' in self.request.GET:
            get_object_or_404(Device, pk=self.request.GET.get('delete')).delete()

            return redirect('devices')

        return super(DevicesPageView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DevicesPageView, self).get_context_data(**kwargs)

        devices = Device.objects.all()
        devices_table = DeviceTable(devices)
        tables.RequestConfig(self.request).configure(devices_table)

        context.update(dict(
            devices=devices_table,
            current_page='devices'
        ))

        return context
