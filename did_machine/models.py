from django.db import models


class Device(models.Model):
    device_label = models.CharField(max_length=60, unique=True)
    hostname = models.CharField(max_length=60, unique=True)
    fs_domain = models.CharField(max_length=60, unique=True)
    provisioned = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s (%s)' % (self.device_label, self.hostname,)

    class Meta:
        ordering = ['provisioned', 'device_label']


class Number(models.Model):
    number = models.CharField(max_length=10, unique=True)

    order = models.CharField(max_length=16)

    fax = models.BooleanField(default=False)
    voicemail = models.BooleanField(default=False)
    conference = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % (self.number,)

    class Meta:
        ordering = ['-pk']
