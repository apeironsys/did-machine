from django.conf.urls import url, include
from django.contrib import admin

from did_machine.views.devices import DevicesPageView
from did_machine.views.numbers import NumbersPageView
from did_machine.views.reservations import NumberReservationsView

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', DevicesPageView.as_view(), name='devices'),
    url(r'^numbers$', NumbersPageView.as_view(), name='numbers'),
    url(r'^reservations$', NumberReservationsView.as_view(), name='reservations'),

    url(r'^freeswitch/', include('freeswitch_restapi.urls')),
]
