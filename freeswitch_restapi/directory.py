from logging import getLogger
from xml.etree.ElementTree import SubElement

from django.conf import settings
from django.http import HttpResponse
from rest_framework import views
from rest_framework.permissions import AllowAny

from did_machine.models import Number
from freeswitch_restapi.util import make_section, pretty_xml

log = getLogger(__name__)

vm_password = settings.EXT_CONFIG.get('freeswitch')['voicemail']['password']
vm_mail_to = settings.EXT_CONFIG.get('freeswitch')['voicemail']['mail_to']
vm_mail_from = settings.EXT_CONFIG.get('freeswitch')['voicemail']['mail_from']


def generate_directory_xml(data):
    document, section = make_section('directory')

    domain = SubElement(section, 'domain', {'name': data.get('domain', '$${domain}')})
    groups = SubElement(domain, 'groups')
    group = SubElement(groups, 'group', {'name': 'default'})
    users = SubElement(group, 'users')

    for did in Number.objects.filter(voicemail=True):
        user = SubElement(users, 'user', {'id': did.number})
        params = SubElement(user, 'params')
        variables = SubElement(user, 'variables')

        # params
        SubElement(params, 'password', {'value': '$${default_password}'})
        SubElement(params, 'vm-password', {'value': '%s' % vm_password})
        SubElement(params, 'vm-mailto', {'value': vm_mail_to})
        SubElement(params, 'vm-mailfrom', {'value': vm_mail_from})
        SubElement(params, 'vm-email-all-messages', {'value': 'true'})
        SubElement(params, 'vm-attach-file', {'value': 'true'})
        SubElement(params, 'http-allowed-api', {'value': 'voicemail'})
        SubElement(params, 'vm-message-ext', {'value': 'wav'})
        SubElement(params, 'directory-visible', {'value': 'false'})

        # variables
        SubElement(variables, 'toll_allow', {'value': 'domestic,international,local'})
        SubElement(variables, 'user_context', {'value': 'default'})
        SubElement(variables, 'effective_caller_id_name', {'value': 'Extension %s' % did.number})
        SubElement(variables, 'effective_caller_id_number', {'value': '%s' % did.number})

    directory = pretty_xml(document)
    log.info('Directory XML: %s', directory)

    return directory


class Directory(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        return self.post(request)

    def post(self, request):
        return HttpResponse(generate_directory_xml(request.data), content_type='text/xml')
