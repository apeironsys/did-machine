from logging import getLogger
from xml.etree.ElementTree import SubElement

from django.http import HttpResponse
from rest_framework import views
from rest_framework.permissions import AllowAny

from did_machine.models import Number
from freeswitch_restapi.dialplan.conference import conference_context
from freeswitch_restapi.dialplan.fax import fax_context
from freeswitch_restapi.dialplan.voicemail import voicemail_context
from freeswitch_restapi.util import make_section, pretty_xml

log = getLogger(__name__)


def generate_dialplan_xml(data):
    document, section = make_section('dialplan')

    context = SubElement(section, 'context', {'name': 'public'})

    if 'Caller-Destination-Number' in data:
        numbers = Number.objects.filter(number=data['Caller-Destination-Number'])
    else:
        numbers = Number.objects.all()

    has_fax = False
    has_voicemail = False
    has_conference = False

    for did in numbers:
        extension = SubElement(context, 'extension', {'name': 'did_%s' % did.number})
        condition = SubElement(extension, 'condition', {'field': 'destination_number',
                                                        'expression': '^%s$' % did.number})

        # figure out which context to transfer the call to
        if did.fax:
            transfer_context = 'fax'
            has_fax = True
        elif did.conference:
            transfer_context = 'conference'
            has_conference = True
        else:  # default context if nothing else is selected
            transfer_context = 'voicemail'
            has_voicemail = True

        # transfer action
        SubElement(condition, 'action', {'application': 'transfer',
                                         'data': '%s XML %s' % (did.number, transfer_context)})

        # hangup as a final action
        # SubElement(condition, 'action', {'application': 'hangup'})

    # voicemail context
    if has_voicemail:
        voicemail_context(section)

    # fax context
    if has_fax:
        fax_context(section)

    # conference context
    if has_conference:
        conference_context(section)

    dialplan = pretty_xml(document)
    log.info('Dialplan XML: %s', dialplan)

    return dialplan


class Dialplan(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        return self.post(request)

    def post(self, request):
        return HttpResponse(generate_dialplan_xml(request.data), content_type='text/xml')
