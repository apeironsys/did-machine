from xml.etree.ElementTree import SubElement
from django.conf import settings


bandwidth = settings.EXT_CONFIG.get('freeswitch')['conference']['bandwidth']
password = settings.EXT_CONFIG.get('freeswitch')['conference']['password']


def conference_context(section):
    conf_context = SubElement(section, 'context', {'name': 'conference'})
    conf_extension = SubElement(conf_context, 'extension', {'name': 'conference'})
    conf_condition = SubElement(conf_extension, 'condition', {'field': 'destination_number',
                                                              'expression': '^([2-9][0-9]{9})$'})
    SubElement(conf_condition, 'action', {'application': 'answer'})
    SubElement(conf_condition, 'action', {'application': 'sleep',
                                          'data': '1000'})
    SubElement(conf_condition, 'action', {'application': 'conference',
                                          'data': '$1@%s+%s' % (bandwidth, password)})
