from xml.etree.ElementTree import SubElement


def fax_context(section):
    context = SubElement(section, 'context', {'name': 'fax'})
    fax_extension = SubElement(context, 'extension', {'name': 'fax'})
    fax_condition = SubElement(fax_extension, 'condition', {'field': 'destination_number',
                                                            'expression': '^([2-9][0-9]{9})$'})
    SubElement(fax_condition, 'action', {'application': 'python',
                                         'data': 'process-rxfax'})
    SubElement(fax_condition, 'action', {'application': 'hangup'})
