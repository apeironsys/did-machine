from xml.etree.ElementTree import SubElement
from django.conf import settings


def voicemail_context(section):
    vm_context = SubElement(section, 'context', {'name': 'voicemail'})
    vm_extension = SubElement(vm_context, 'extension', {'name': 'voicemail'})
    vm_condition = SubElement(vm_extension, 'condition', {'field': 'destination_number',
                                                          'expression': '^([2-9][0-9]{9})$'})
    SubElement(vm_condition, 'action', {'application': 'export',
                                        'data': 'dialed_extension=$1'})
    SubElement(vm_condition, 'action', {'application': 'bind_meta_app',
                                        'data': '1 b s execute_extension::dx XML features'})
    SubElement(vm_condition, 'action', {'application': 'bind_meta_app',
                                        'data': '2 b s record_session::$${recordings_dir}/${caller_id_number}.${strftime(%Y-%m-%d-%H-%M-%S)}.wav'})
    SubElement(vm_condition, 'action', {'application': 'bind_meta_app',
                                        'data': '3 b s execute_extension::cf XML features'})
    SubElement(vm_condition, 'action', {'application': 'bind_meta_app',
                                        'data': '4 b s execute_extension::att_xfer XML features'})
    SubElement(vm_condition, 'action', {'application': 'set',
                                        'data': 'ringback=${us-ring}'})
    SubElement(vm_condition, 'action', {'application': 'set',
                                        'data': 'transfer_ringback=$${hold_music}'})
    SubElement(vm_condition, 'action', {'application': 'set',
                                        'data': 'call_timeout=30'})
    SubElement(vm_condition, 'action', {'application': 'set',
                                        'data': 'hangup_after_bridge=true'})
    SubElement(vm_condition, 'action', {'application': 'set',
                                        'data': 'continue_on_fail=true'})
    SubElement(vm_condition, 'action', {'application': 'hash',
                                        'data': 'insert/${domain_name}-call_return/${dialed_extension}/${caller_id_number}'})
    SubElement(vm_condition, 'action', {'application': 'hash',
                                        'data': 'insert/${domain_name}-last_dial_ext/${dialed_extension}/${uuid}'})
    SubElement(vm_condition, 'action', {'application': 'set',
                                        'data': 'called_party_callgroup=${user_data(${dialed_extension}@${domain_name} var callgroup)}'})
    SubElement(vm_condition, 'action', {'application': 'hash',
                                        'data': 'insert/${domain_name}-last_dial_ext/${called_party_callgroup}/${uuid}'})
    SubElement(vm_condition, 'action', {'application': 'hash',
                                        'data': 'insert/${domain_name}-last_dial_ext/global/${uuid}'})
    SubElement(vm_condition, 'action', {'application': 'hash',
                                        'data': 'insert/${domain_name}-last_dial/${called_party_callgroup}/${uuid}'})
    SubElement(vm_condition, 'action', {'application': 'bridge',
                                        'data': 'user/${dialed_extension}@${domain_name}'})
    SubElement(vm_condition, 'action', {'application': 'answer'})
    SubElement(vm_condition, 'action', {'application': 'sleep',
                                        'data': '1000'})
    SubElement(vm_condition, 'action', {'application': 'bridge',
                                        'data': 'loopback/app=voicemail:default ${domain_name} $1'})
    SubElement(vm_condition, 'action', {'application': 'hangup'})
