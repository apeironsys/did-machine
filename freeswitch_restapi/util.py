from xml.dom import minidom
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement


def make_section(name):
    document = Element('document', {'type': 'freeswitch/xml'})
    return document, SubElement(document, 'section', {'name': name})


def pretty_xml(elem):
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent='  ')
