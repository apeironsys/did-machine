from django.conf.urls import url

from freeswitch_restapi.dialplan import Dialplan
from freeswitch_restapi.directory import Directory

urlpatterns = [
    url(r'^dialplan.xml$', Dialplan.as_view(), name='freeswitch_dialplan'),
    url(r'^directory.xml$', Directory.as_view(), name='freeswitch_directory'),
]
